package com.thinkncync.cryptolib;

public interface CryptoPolicy {
    void setSecretKey(String key);
    String encrypt(String value);
    String decrypt(String value);
}
