package com.thinkncync.cryptolib;

import android.util.Base64;

import androidx.annotation.Nullable;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class AesBase64CryptoPolicyImpl implements CryptoPolicy {

    private String key;

    public AesBase64CryptoPolicyImpl(String secretKey) {
        setSecretKey(secretKey);
    }

    @Override
    public void setSecretKey(String key) {
        this.key = key;
    }

    @Override
    public String encrypt(String value){
        try {
            Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
            if(cipher != null) {
                byte[] encoded = cipher.doFinal(value.getBytes());
                return new String(Base64.encode(encoded, Base64.DEFAULT));
            }
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String decrypt(String encryptedText){
        try {
            Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
            if(cipher != null) {
                byte[] base64Text = Base64.decode(encryptedText.getBytes(), Base64.DEFAULT);
                return new String(cipher.doFinal(base64Text));
            }
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Nullable
    private Cipher getCipher(int cipherMode){
        try {
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(cipherMode, aesKey);
            return cipher;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }
}
